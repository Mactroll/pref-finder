//
//  Preferences.swift
//  pref-finder
//
//  Created by Joel Rennich on 3/24/17.
//  Copyright © 2017 Trusource Labs. All rights reserved.
//

import Foundation
import CoreFoundation

class Preferences {

    var defaults: UserDefaults?
    var allItems: NSDictionary?

    init( domain: String) {
        defaults = UserDefaults.init(suiteName: domain)
        allItems = defaults?.dictionaryRepresentation() as NSDictionary?
        //let allTheDefaults = CFPreferencesCopyMultiple(nil, kCFPreferencesAnyApplication, kCFPreferencesAnyUser, kCFPreferencesAnyHost)
            //CFPreferencesCopyKeyList(kCFNull, kCFPreferencesAnyUser, kCFPreferencesAnyHost)
        //print(allTheDefaults)
    }

    func printAll() {
        print(defaults?.dictionaryRepresentation())
    }

    func printKey(key: String) {
        print(defaults?.string(forKey: key))
    }

    func checkForced(key: String) -> Bool {
        return (defaults?.objectIsForced(forKey: key))!
    }

    func checkForcedAll() {
        print("Forced Keys:")
        for item in allItems! {
            if (defaults?.objectIsForced(forKey: item.key as! String))! {
                print(" " + (item.key as! String) + " is forced to: " + String(describing: item.value))
            }
        }
    }
}
